<?php


use Slim\Http\Request;
use Slim\Http\Response;

// Fungsi Menampilkan service






function getPenyakit(Request $request, Response $response) {
    $sql = "SELECT * FROM `penyakit`";
		$db = getConnection();
		$stmt = $db->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
	    return $response->withJson(["status" => "success", "data" => $result], 200);
		
	
}


function getPenyakitNama(Request $request, Response $response, $args) {
	$nama = $args["nama"];
    $sql = "SELECT * FROM penyakit WHERE namaPenyakit=:nama ";
	$db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->execute([":nama" => $nama]);
	$db = null;
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $response->withJson(["status" => "success", "data" => $result], 200);

}



function getPenyakitSearch(Request $request, Response $response, $args){
    $keyword = $request->getQueryParam("keyword");
    $sql = "SELECT * FROM penyakit WHERE namaPenyakit LIKE '%$keyword%' ";
	$db = getConnection();
	$stmt = $db->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $response->withJson(["status" => "success", "data" => $result], 200);
}


function postPenyakit(Request $request, Response $response){
	$new_record = $request->getParsedBody();
    $sql = "INSERT INTO penyakit (idPenyakit, namaPenyakit, gejala, penanggulangan, tipsPencegahan) VALUE (:idPenyakit, :namaPenyakit, :gejala, :penanggulangan, :tipsPencegahan )";
    $db = getConnection();
	$stmt = $db->prepare($sql);
    $data = [
		":idPenyakit" => $new_record["idPenyakit"],
        ":namaPenyakit" => $new_record["namaPenyakit"],
        ":gejala" => $new_record["gejala"],
        ":penanggulangan" => $new_record["penanggulangan"],
		":tipsPencegahan" => $new_record["tipsPencegahan"]
    ];

    if($stmt->execute($data))
       return $response->withJson(["status" => "success", "data" => "1"], 200);
    
    return $response->withJson(["status" => "failed", "data" => "0"], 200);
}


function delPenyakit(Request $request, Response $response, $args){ 
	$idPenyakit = $args["idPenyakit"];
    $sql = "DELETE FROM penyakit WHERE idPenyakit=:idPenyakit";
	$db = getConnection();
    $stmt = $db->prepare($sql);
    
    $data = [
        ":id" => $id
    ];

    if($stmt->execute($data))
       return $response->withJson(["status" => "success", "data" => "1"], 200);
    
    return $response->withJson(["status" => "failed", "data" => "0"], 200);
}




function putPenyakit(Request $request, Response $response, $args){
    $idPenyakit = $args["idPenyakit"];
    $new_record = $request->getParsedBody();
    $sql = "UPDATE penyakit SET idPenyakit=:idPenyakit, namaPenyakit=:namaPenyakit, gejala=:gejala, penanggulangan=:penanggulangan, tipsPencegahan=:tipsPencegahan WHERE idPenyakit=:idPenyakit";
    $db = getConnection();
    $stmt = $db->prepare($sql);
    
   $data = [
		":idPenyakit" => $idPenyakit,
		":namaPenyakit" => $new_record["namaPenyakit"],
        ":gejala" => $new_record["gejala"],
        ":penanggulangan" => $new_record["penanggulangan"],
		":tipsPencegahan" => $new_record["tipsPencegahan"]
    ];

    if($stmt->execute($data))
       return $response->withJson(["status" => "success", "data" => "1"], 200);
    
    return $response->withJson(["status" => "failed", "data" => "0"], 200);
}

?>