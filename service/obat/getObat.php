<?php

require_once('config/setting.php');

function getObat($response) {
  $query = "SELECT kode_produk,nama,type,harga,min_stok,expired1 FROM public.obat ORDER BY min_stok";
  try {
    $result = getConnection()->query($query);
    // var_dump($result);
    while ($row = $result->fetchAll(PDO::FETCH_OBJ)){
      $data = $row;
    }
    return json_encode($data);
  } catch(PDOException $e) {
    echo '{"error":{"text":'. $e->getMessage() .'}}';
  }
}


//get ID obat

function getDrugID($request,$response) {
  $id_obat = $request->getAttribute('id');
  $query = "SELECT id_obat,kode_produk,nama,type,harga,min_stok,expired1 FROM obat WHERE  id_obat = :id_obat";
  try {
    // $result = getConnection()->query($query);
    $result = getConnection()->prepare($query);
    $result->bindParam("id_obat", $id_obat);
    $result->execute();
    // var_dump($result);
    while ($row = $result->fetchAll(PDO::FETCH_OBJ)){
      $data = $row;
    }
    return json_encode($data);
  } catch(PDOException $e) {
    echo '{"error":{"text":'. $e->getMessage() .'}}';
  }
}


//update stock obat

function updateSHDrugsStock($request) {
	
  $data = json_decode(json_encode($request->getParsedBody()));
  $id_obat = $request->getAttribute('id');
 
		
  $query = "UPDATE obat 
			SET harga = :harga, min_stok = :min_stok, expired1= :expired1
			WHERE id_obat= :id_obat";
  $query1 = "UPDATE people SET first_name = :firstname, last_name = :lastname WHERE id_num = :numid AND status = 1";
  try {
    $stmt = getConnection()->prepare($query);
    $stmt->bindParam("harga", $data->harga);
    $stmt->bindParam("min_stok", $data->min_stok);
	$stmt->bindParam("expired1", $data->expired1);
    $stmt->bindParam("id_obat", $id_obat);
    $stmt->execute();
    echo json_encode($data);
	

  } catch(PDOException $e) {
    echo '{"error":{"text":'. $e->getMessage() .'}}';
  }
}



