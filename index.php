<?php


require 'vendor/autoload.php';
require 'service/welcome.php';
require 'service/medrec/Medrec.php';
require 'service/obat/getObat.php';
require 'service/pasien/getSHPatientID.php';
require 'service/penyakit/penyakit.php';

$app = new Slim\App();
//$dotenv = Dotenv\Dotenv::create(__DIR__);
//$dotenv->load();
$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

	$app->group('/api', function () use ($app) {
    $app->get('/', 'welcome');
    $app->get('/obat', 'getObat');
	
	$app->get('/medrec/', 'getMedrec');
	$app->get('/medrec', 'getMedrec');
	$app->get('/medrec/{id}', 'getMedrecId');
	$app->get('/medrec/search/', 'getMedrecSearch');
	$app->get('/medrec/tgl/', 'getMedrecTgl');
	$app->post('/medrec/', 'postMedrec');
	$app->delete('/medrec/{id}', 'delMedrec');
	$app->put('/medrec/{id}', 'putMedrec');
	
	
	
	$app->get('/penyakit/', 'getPenyakit');
	$app->get('/penyakit', 'getPenyakit');
	$app->get('/penyakit/{nama}', 'getPenyakitNama');
	$app->get('/penyakit/search/', 'getPenyakitSearch');
	$app->post('/penyakit/', 'postPenyakit');
	$app->delete('/penyakit/{idPenyakit}', 'delPenyakit');
	$app->put('/penyakit/{idPenyakit}', 'putPenyakit');
	
	$app->get('/obat/{id}','getDrugID');
	$app->put('/obat/update/{id}','updateSHDrugsStock');
	$app->get('/pasien','getSHPatientID');
   
   
  });

 
  
// Register middleware
require __DIR__ . '/config/middleware.php'; 
  

  
$app->run();
?>