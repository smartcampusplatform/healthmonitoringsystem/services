# Sample PHP Services


Smart health merupakan program API yang menyediakan layanan untuk Bumi
Medika Ganesha. Adapun service yang disediakan adalah:
1. Layanan Registrasi Pasien Baru
2. Layanan Pendaftaran Antrian
3. Layanan Rekam Medis
4. Layanan Pembayaran
5. Layanan Pengambilan Obat
6. Layanan Informasi Kesehatan

---

* [Language](#language)
Untuk membuat service-service yang ada pada Smart Health, digunakan bahasa
pemrograman PHP dengan framework Slim. 

* [List of SmartHealth Service](#list-of-SmartHealth-Services)
  - [Getting All Pasein](#getting-all-pasien)
  - [Getting Satu Paseien](#getting-satu-pasien)
  - [Registrasi Pasien](#registrasi-pasien)
  - [Updating pasien](#updating-pasien)
  - [Deleting pasien](#deleting-pasien)
  
* [List of Antrian services](#list-of-Antrian-services)
  - [Input Antrian](#input-antrian)
  
  
* [Database](#database)
smarthealth

* [Built With](#built-with)

* [Authors](#authors)
Hespri Yomeldi (23218053)
Nova Nurviana (23218048)
Nuqson Masykur Huda (23218072)

* [License](#license)

* [Acknowledgments](#acknowledgments)
Mr Dimas
 

---

## Language

This services are written in **PHP** dengan framework SLIM

## database
database yang digunakan postgres dalam, configurasi dalam config\setting.php
file smart_health.sql
## List of API Services

### **Pendaftaran Pasien Baru**

---

API for getting all Pasien.

* **URL**

  ```
  api/pasien/
  ```

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[{"id_pasien":"id","nama":"nama_pasien","HP":"telepon","alamat":"alamat"}]`

* **Sample Call:**

  ```
  curl -i /api/pasien
  ```

### **Pasien Identitas**

---

API for getting single pasien by id.

* **URL**

  ```
  api/pasien/:id
  ```

* **Method:**

  `GET`

* **URL Params**

  URL params id needs to be specified.

  **Required:**

  `id=[integer]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[{"id_pasien":"id","nama":"nama","HP":"telepon","alamat":"alamat"}]`

* **Sample Call:**

  ```
  curl -i /api/pasien/id
  ```

### **Registrasi Pasien**

---

API for registrasi pasien.

* **URL**

  ```
  /api/pasien/id
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `id_pasien=[integer], nama=[string], alamat=[string]`

* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  $.ajax({
    url: "/api/pasien/id",
    type: "POST",
    dataType: "application/json",
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
      // function if success
    },
    error: function (err) {
      // function if error
    }
  });
  ```

### **Layanan Pengambilan Obat**

---

API for updating person.

* **URL**

  ```
  api/drugs/stock/update/:id
  ```

* **Method:**

  `PUT`

* **URL Params**

  URL params id needs to be specified.

  **Required:**

  `id=[integer]`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `firstname=[string], lastname=[string]`

* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  $.ajax({
    url: "/api/drug/update/123",
    type: "PUT",
    dataType: "application/json",
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
      // function if success
    },
    error: function (err) {
      // function if error
    }
  });
  ```

### **Layanan Pembayaran**

---

API for deleting person.

* **URL**

  ```
  /api/payment/:id
  ```

* **Method:**

  `PUT`

* **URL Params**

  URL params id needs to be specified.

  **Required:**

  `id=[integer]`

* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  $.ajax({
    url: "/api/paymnent/",
    type: "PUT",
    dataType: "application/json",
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
      // function if success
    },
    error: function (err) {
      // function if error
    }
  });
  ```
### **Informasi Kesehatan**

---

API untuk menampilkan medical record

* **URL**

  ```
  api/medrec/
  ```

* **Method:**

  `GET`

* **URL Params**

  URL params id needs to be specified. untuk validasi middleware

  **Required:**

  `key=[integer]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[{"idmedrec":"idmedrec","id_pasien":"id_pasien","patientweight":"patientweight","patientheight":"patientheight","patienttensi":"patienttensi","patientdiagnose":"patientdiagnose","datemedical":"datemedical"}]`

* **Sample Call:**

  ```
  curl /api/medrec/?key=123
  ```
API untuk menampilkan info penyakit

* **URL**

  ```
  api/penyakit/
  ```

* **Method:**

  `GET`

* **URL Params**

  URL params id needs to be specified. untuk validasi middleware

  **Required:**

  `key=[integer]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[{"idpenyakit":"idpenyakit","namapenyakit":"namapenyakit","gejala":"gejala","penanggulangan":"penanggulangan","tipspencegahan":"tipspencegahan"}]`

* **Sample Call:**

  ```
  curl /api/penyakit/?key=123
  ```