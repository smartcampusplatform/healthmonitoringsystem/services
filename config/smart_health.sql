/*
 Navicat Premium Data Transfer

 Source Server         : post
 Source Server Type    : PostgreSQL
 Source Server Version : 110002
 Source Host           : localhost:5432
 Source Catalog        : smart_health
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110002
 File Encoding         : 65001

 Date: 13/05/2019 14:21:19
*/


-- ----------------------------
-- Table structure for antrian_pasien
-- ----------------------------
DROP TABLE IF EXISTS "public"."antrian_pasien";
CREATE TABLE "public"."antrian_pasien" (
  "id" int4 NOT NULL DEFAULT nextval('antrian_sequence'::regclass),
  "date" timestamp(6),
  "id_pasien" int4,
  "sm_code" varchar(250) COLLATE "pg_catalog"."default",
  "nama" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for dokter
-- ----------------------------
DROP TABLE IF EXISTS "public"."dokter";
CREATE TABLE "public"."dokter" (
  "id_dokter" int4 NOT NULL,
  "nama" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "alamat" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "handpone" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "phone" varchar(13) COLLATE "pg_catalog"."default" NOT NULL,
  "id_spesialis" int4 NOT NULL
)
;

-- ----------------------------
-- Records of dokter
-- ----------------------------
INSERT INTO "public"."dokter" VALUES (1, 'Budi Budiman', 'jalan jalan jalan jalanjalan jalan jalan jalan jal', '811584986', '620095', 7);
INSERT INTO "public"."dokter" VALUES (3, 'Susan', 'sdadasd', '2147483647', '637793', 2);
INSERT INTO "public"."dokter" VALUES (4, 'Joko jokoko', 'asddasdasdasdasdasdasddasdasdasasasdsadsadsd asdda', '811294263', '646123', 3);
INSERT INTO "public"."dokter" VALUES (8, 'Agus Sulaiman', 'jl. mawar no.10
Lippo Karawaci
Tangerang
', '2147483647', '2147483647', 1);
INSERT INTO "public"."dokter" VALUES (10, 'Arif Setiawan', 'Jakarta', '2147483647', '567', 1);

-- ----------------------------
-- Table structure for medrec
-- ----------------------------
DROP TABLE IF EXISTS "public"."medrec";
CREATE TABLE "public"."medrec" (
  "idmedrec" varchar(8) COLLATE "pg_catalog"."default" NOT NULL,
  "id_pasien" int4 NOT NULL,
  "patientweight" int4 NOT NULL,
  "patientheight" int4 NOT NULL,
  "patienttensi" int4 NOT NULL,
  "patientdiagnose" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "datemedical" date NOT NULL
)
;

-- ----------------------------
-- Records of medrec
-- ----------------------------
INSERT INTO "public"."medrec" VALUES ('MR1', 1, 65, 130, 120, 'cancer', '2019-04-02');
INSERT INTO "public"."medrec" VALUES ('MR2', 2, 450, 170, 110, 'tubercolosis', '2019-04-02');
INSERT INTO "public"."medrec" VALUES ('MR3', 3, 45, 180, 110, 'tubercolosis', '2019-04-02');
INSERT INTO "public"."medrec" VALUES ('MR4', 4, 65, 130, 100, 'diabetes', '2019-04-02');
INSERT INTO "public"."medrec" VALUES ('MR5', 5, 40, 130, 100, 'diabetes', '2019-04-01');
INSERT INTO "public"."medrec" VALUES ('MR6', 2, 40, 130, 100, 'diabetes', '2019-04-09');
INSERT INTO "public"."medrec" VALUES ('MR7', 2, 40, 130, 100, 'Influenza', '2019-04-09');
INSERT INTO "public"."medrec" VALUES ('MR8', 3, 12, 33, 33, 'tubercolosis', '2019-05-20');
INSERT INTO "public"."medrec" VALUES ('MR9', 1, 45, 160, 111, 'tubercolosis', '2019-04-09');
INSERT INTO "public"."medrec" VALUES ('MR10', 3, 50, 160, 140, 'influenza', '2019-05-12');
INSERT INTO "public"."medrec" VALUES ('MR11', 7, 70, 180, 130, 'cancer', '2019-05-02');
INSERT INTO "public"."medrec" VALUES ('MR13', 6, 40, 150, 123, 'cancer', '2019-03-23');
INSERT INTO "public"."medrec" VALUES ('MR14', 6, 40, 150, 123, 'cancer', '2019-03-23');
INSERT INTO "public"."medrec" VALUES ('MR15', 2, 120, 34, 60, 'cancer', '2019-03-26');
INSERT INTO "public"."medrec" VALUES ('MR16', 2, 40, 130, 200, 'Influenza', '2019-04-05');
INSERT INTO "public"."medrec" VALUES ('MR17', 4, 40, 130, 100, 'Influenza', '2019-04-05');
INSERT INTO "public"."medrec" VALUES ('MR18', 34, 54, 5, 5, 'h', '2019-05-13');
INSERT INTO "public"."medrec" VALUES ('MR19', 5, 45, 165, 123, 'tubercolosis', '2019-05-13');

-- ----------------------------
-- Table structure for obat
-- ----------------------------
DROP TABLE IF EXISTS "public"."obat";
CREATE TABLE "public"."obat" (
  "id_obat" int4 NOT NULL,
  "kode_produk" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "nama" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "type" int4 NOT NULL,
  "quantity" int4 NOT NULL,
  "unit" char(15) COLLATE "pg_catalog"."default" NOT NULL,
  "keterangan" varchar(500) COLLATE "pg_catalog"."default" NOT NULL,
  "harga" int4 NOT NULL,
  "max_stok" int4 NOT NULL,
  "min_stok" int4 NOT NULL,
  "quantity1" int4,
  "expired1" date,
  "quantity2" int4,
  "expired2" date
)
;

-- ----------------------------
-- Records of obat
-- ----------------------------
INSERT INTO "public"."obat" VALUES (2, 'PAN19052016', 'Panadol', 2, 45, 'KAP            ', 'obat penghilang sakit kaki', 50000, 200, 50, NULL, NULL, NULL, NULL);
INSERT INTO "public"."obat" VALUES (3, 'PAR20032998', 'Paramexs', 2, -290, 'TAB            ', 'pereda sakit tenggorokan dan kepala', 75000, 100, 20, NULL, NULL, NULL, NULL);
INSERT INTO "public"."obat" VALUES (4, 'BOD12042020', 'Bodrex', 2, -377, 'TAB            ', 'bodrex obat sakit kepala berat', 20000, 500, 200, NULL, NULL, NULL, NULL);
INSERT INTO "public"."obat" VALUES (5, 'LIP20052990', 'Lipitor 40 MG', 2, -527, 'BUT            ', 'Obat kolestrol', 20000, 400, 150, NULL, NULL, NULL, NULL);
INSERT INTO "public"."obat" VALUES (6, 'YOS10202012', 'Yosenob 300', 2, 180, 'BUT            ', 'obat penurun kolestrol', 6700, 400, 50, NULL, NULL, NULL, NULL);
INSERT INTO "public"."obat" VALUES (7, 'EVO19052897', 'Evothyl 300', 2, -546, 'BUT            ', 'obat penurun kolestrol ringan', 8250, 500, 30, NULL, NULL, NULL, NULL);
INSERT INTO "public"."obat" VALUES (8, 'FIB12052009', 'Fibramed 300 MG 30 ', 2, -1139, 'TAB            ', 'Jamu pereda sakit', 50000, 500, 150, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for pasien
-- ----------------------------
DROP TABLE IF EXISTS "public"."pasien";
CREATE TABLE "public"."pasien" (
  "id_pasien" int4 NOT NULL,
  "no_pasien" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "nama" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "umur" int4 NOT NULL,
  "jenis_kelamin" char(10) COLLATE "pg_catalog"."default" NOT NULL,
  "tanggal" date NOT NULL,
  "alamat" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "telpon" varchar(13) COLLATE "pg_catalog"."default" NOT NULL,
  "telpon_rumah" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "tanggal_daftar" date NOT NULL
)
;

-- ----------------------------
-- Records of pasien
-- ----------------------------
INSERT INTO "public"."pasien" VALUES (1, '000000001', 'Agus', 32, 'pria      ', '1996-02-19', 'Jalan Merpati No.20Jakarta UtaraIndonesia', '0', '18648646', '2016-02-19');
INSERT INTO "public"."pasien" VALUES (2, '000000415', 'Hadi Supriadi', 45, 'pria      ', '1997-03-10', 'Kutilang Raya No.10KarawaciTangerang', '4452589', '11846225', '2016-03-08');
INSERT INTO "public"."pasien" VALUES (3, '000000002', 'Neni', 25, 'Wanita    ', '1995-06-03', 'Jalan Pondok Jati No A1', '4452576', '11842225', '2016-03-08');
INSERT INTO "public"."pasien" VALUES (4, '000000003', 'Mahardika', 17, 'Laki-laki ', '1989-03-23', 'Jalan Grabagan no 34', '1452576', '12842225', '2016-04-08');
INSERT INTO "public"."pasien" VALUES (5, '000000004', 'Asmaul Qoinah', 40, 'Wanita    ', '1975-08-06', 'Jalan Melati Raya', '4452516', '11842215', '2016-03-12');
INSERT INTO "public"."pasien" VALUES (6, '000000005', 'Naning', 15, 'Wanita    ', '2000-06-03', 'Jalan Melati Raya', '4452536', '11842222', '2016-03-20');
INSERT INTO "public"."pasien" VALUES (7, '000000006', 'Maya', 37, 'Wanita    ', '0193-01-11', 'Jalan Pasteur no 33A', '1453576', '41842425', '2016-04-10');
INSERT INTO "public"."pasien" VALUES (8, '000000007', 'Evi Wijayanti', 62, 'Wanita    ', '1963-07-07', 'Jalan Pasar Dalam', '4452586', '11942225', '2016-04-08');
INSERT INTO "public"."pasien" VALUES (9, '000000008', 'Joko Santoso', 42, 'pria      ', '1963-07-27', 'Jalan Simpang 3 No 44', '4152176', '23842225', '2016-03-28');
INSERT INTO "public"."pasien" VALUES (10, '000000009', 'Yono Sujiwo', 36, 'pria      ', '1985-06-05', 'Jalan Cikutra RayaNo 77', '4452876', '11847825', '2016-04-08');

-- ----------------------------
-- Table structure for penyakit
-- ----------------------------
DROP TABLE IF EXISTS "public"."penyakit";
CREATE TABLE "public"."penyakit" (
  "idpenyakit" varchar(10) COLLATE "pg_catalog"."default" NOT NULL,
  "namapenyakit" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "gejala" text COLLATE "pg_catalog"."default" NOT NULL,
  "penanggulangan" text COLLATE "pg_catalog"."default" NOT NULL,
  "tipspencegahan" text COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of penyakit
-- ----------------------------
INSERT INTO "public"."penyakit" VALUES ('DS4', 'cancer', '1. Muncul benjolan yang tidak lazim
2. Perubahan pada kulit
3. Masalah pada kelenjar getah bening
4. Berat badan turun tanpa sebab
5. Batuk atau sesak yang berkepanjangan
6. Rasa sakit tanpa sebab
7. Perdarahan tidak normal', '1. Kemoterapi
2. Radioterapi
3. Terapi target', '1.Jangan merokok.
2.Makanlah dengan pola makan yang sehat.
3.Makanlah banyak buah-buahan dan sayuran.
4.Batasilah daging olahan.
5.Pertahankanlah berat badan yang sehat dan aktif secara fisik. 
6.Lindungilah diri dari sinar matahari.');
INSERT INTO "public"."penyakit" VALUES ('DS2', 'diabetes', '1. Cepat haus
2. Banyak buang air kecil
3. Cepat lapar
4. Penurunan berat badan 
5. Rasa lelah dan lemah yang tidak biasa
6. Pandangan kabur
7. Pemulihan luka yang lama atau sering infeksi
8. Warna kulit gelap', '1. Makan makanan sehat
2. Patuhi pola makan yang diberikan dokter
3. Olahraga teratur
4. Tes gula darah Anda setiap hari
5. Pastikan Anda selalu minum obat atau suntik insulin', '1.Mengurangi porsi makan
2.Olahraga
3.Menurunkan berat badan
4.Sarapan sangat penting
5.Hindari makanan berlemak
6.Hindari minuman manis
7.Makan banyak sayuran
8.Tidur nyenyak');
INSERT INTO "public"."penyakit" VALUES ('DS3', 'influenza', '1. Demam
2. Badan Pans
3. HIdung meler
4. Batuk
5. Tenggorokan sakit
6. Sakit Kepala', '1. Perbanyak minum putih hangat
2. Minum vit C
3. Istirahat Cukup
4. Minum Paracetamol
5. Minuman hangat (jahe)', '1. Banyak minum air putih
2. Jaga kondisi tubuh
3. Minum Vit C
');
INSERT INTO "public"."penyakit" VALUES ('DS1', 'tubercolosis', '1.batuk berdahak yang berlangsung selama lebih dari 21 hari
2. Batuk juga terkadang dapat mengeluarkan darah
3. kehilangan nafsu makan 
4. badan yang disertai demam dan kelelahan', 'Tuberkulosis termasuk penyakit yang sulit untuk dideteksi, terutama pada anak- anak. Dokter biasanya menggunakan beberapa cara untuk mendiagnosis penyakit ini, antara lain rontgen dada, tes Mantoux, tes darah, dan tes dahak. Penyakit yang tergolong serius ini dapat disembuhkan jika diobati dengan benar. Langkah pengobatan yang dibutuhkan adalah dengan mengonsumsi beberapa jenis antibiotik yang harus diminum selama jangka waktu tertentu.', '1.Langkah utama dalam pencegahan TB adalah dengan menerima vaksin BCG (Bacillus Calmette-Guerin).
2.hindari terjangkit HIV/AIDS, diabetes atau orang yang sedang menjalani kemoterapi.
3.Hindari mengalami malanutrisi atau kekurangan gizi.
4.hindari narkoba.
5. Hindari  merokok.
');

-- ----------------------------
-- Table structure for spesialis
-- ----------------------------
DROP TABLE IF EXISTS "public"."spesialis";
CREATE TABLE "public"."spesialis" (
  "id_spesialis" int4 NOT NULL,
  "nama" varchar(50) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of spesialis
-- ----------------------------
INSERT INTO "public"."spesialis" VALUES (1, 'Tulangsx');
INSERT INTO "public"."spesialis" VALUES (2, 'Kepala');
INSERT INTO "public"."spesialis" VALUES (3, 'Kaki');
INSERT INTO "public"."spesialis" VALUES (6, 'Jantung');
INSERT INTO "public"."spesialis" VALUES (7, 'Paru - paru');
INSERT INTO "public"."spesialis" VALUES (8, 'Ginjal');
INSERT INTO "public"."spesialis" VALUES (9, 'tangan');
INSERT INTO "public"."spesialis" VALUES (10, 'Nell Shepherd');

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."supplier";
CREATE TABLE "public"."supplier" (
  "id_supplier" int4 NOT NULL,
  "nama" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "alamat" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "telpon" varchar(15) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of supplier
-- ----------------------------
INSERT INTO "public"."supplier" VALUES (1, 'PT. Paramex Indonesiae', 'Jakarta Utara no.10', '2147483647');
INSERT INTO "public"."supplier" VALUES (3, 'supplier panadol', 'jalan jalan', '486486');
INSERT INTO "public"."supplier" VALUES (4, 'PT. Panadol Jakarta', 'Kebon Jeruk No.10
Sumatra', '411255436');
INSERT INTO "public"."supplier" VALUES (5, 'Tasha Elliott', 'Fugiat eius consequat Quis ea dolorem ut id est ', '2147483647');
INSERT INTO "public"."supplier" VALUES (6, 'Hedda Manning', 'Voluptates quisquam ut eligendi atque non tempor o', '2147483647');

-- ----------------------------
-- Table structure for tiket
-- ----------------------------
DROP TABLE IF EXISTS "public"."tiket";
CREATE TABLE "public"."tiket" (
  "id_tiket" int4 NOT NULL,
  "nama" char(100) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of tiket
-- ----------------------------
INSERT INTO "public"."tiket" VALUES (1, '3x1                                                                                                 ');
INSERT INTO "public"."tiket" VALUES (2, '2x1                                                                                                 ');
INSERT INTO "public"."tiket" VALUES (3, '5x1                                                                                                 ');
INSERT INTO "public"."tiket" VALUES (4, '12x1                                                                                                ');

-- ----------------------------
-- Table structure for tiket_waktu
-- ----------------------------
DROP TABLE IF EXISTS "public"."tiket_waktu";
CREATE TABLE "public"."tiket_waktu" (
  "id_tiket_waktu" int4 NOT NULL,
  "nama" char(100) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of tiket_waktu
-- ----------------------------
INSERT INTO "public"."tiket_waktu" VALUES (1, 'Sebelum Makans                                                                                      ');
INSERT INTO "public"."tiket_waktu" VALUES (2, 'Sesudah Makan                                                                                       ');

-- ----------------------------
-- Table structure for tipe_obat
-- ----------------------------
DROP TABLE IF EXISTS "public"."tipe_obat";
CREATE TABLE "public"."tipe_obat" (
  "id_tipe" int4 NOT NULL,
  "nama" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tipe_obat
-- ----------------------------
INSERT INTO "public"."tipe_obat" VALUES (1, 'Obat Luar');
INSERT INTO "public"."tipe_obat" VALUES (2, 'Obat Dalam');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS "public"."user";
CREATE TABLE "public"."user" (
  "id_user" int4 NOT NULL,
  "username" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "level" char(10) COLLATE "pg_catalog"."default" NOT NULL,
  "status" int4 NOT NULL,
  "api_key" varchar(50) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO "public"."user" VALUES (1, 'admin', '202cb962ac59075b964b07152d234b70', 'admin     ', 1, '123');
INSERT INTO "public"."user" VALUES (5, 'hes', '202cb962ac59075b964b07152d234b70', 'admin     ', 1, '123');
INSERT INTO "public"."user" VALUES (7, 'nova', 'c4ca4238a0b923820dcc509a6f75849b', 'admin     ', 1, '123');
INSERT INTO "public"."user" VALUES (6, 'huda', '202cb962ac59075b964b07152d234b70', 'admin     ', 1, '6cec4eb5e4af00822c95d5cdb537d259');

-- ----------------------------
-- Primary Key structure for table antrian_pasien
-- ----------------------------
ALTER TABLE "public"."antrian_pasien" ADD CONSTRAINT "antrian_pasien_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table dokter
-- ----------------------------
ALTER TABLE "public"."dokter" ADD CONSTRAINT "dokter_pkey" PRIMARY KEY ("id_dokter");

-- ----------------------------
-- Indexes structure for table medrec
-- ----------------------------
CREATE INDEX "idMedrec" ON "public"."medrec" USING btree (
  "idmedrec" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table medrec
-- ----------------------------
ALTER TABLE "public"."medrec" ADD CONSTRAINT "medrec_pkey" PRIMARY KEY ("idmedrec");

-- ----------------------------
-- Primary Key structure for table obat
-- ----------------------------
ALTER TABLE "public"."obat" ADD CONSTRAINT "obat_pkey" PRIMARY KEY ("id_obat");

-- ----------------------------
-- Primary Key structure for table pasien
-- ----------------------------
ALTER TABLE "public"."pasien" ADD CONSTRAINT "pasien_pkey" PRIMARY KEY ("id_pasien");

-- ----------------------------
-- Primary Key structure for table penyakit
-- ----------------------------
ALTER TABLE "public"."penyakit" ADD CONSTRAINT "penyakit_pkey" PRIMARY KEY ("idpenyakit");

-- ----------------------------
-- Primary Key structure for table spesialis
-- ----------------------------
ALTER TABLE "public"."spesialis" ADD CONSTRAINT "spesialis_pkey" PRIMARY KEY ("id_spesialis");

-- ----------------------------
-- Primary Key structure for table supplier
-- ----------------------------
ALTER TABLE "public"."supplier" ADD CONSTRAINT "supplier_pkey" PRIMARY KEY ("id_supplier");

-- ----------------------------
-- Primary Key structure for table tiket
-- ----------------------------
ALTER TABLE "public"."tiket" ADD CONSTRAINT "tiket_pkey" PRIMARY KEY ("id_tiket");

-- ----------------------------
-- Primary Key structure for table tiket_waktu
-- ----------------------------
ALTER TABLE "public"."tiket_waktu" ADD CONSTRAINT "tiket_waktu_pkey" PRIMARY KEY ("id_tiket_waktu");

-- ----------------------------
-- Primary Key structure for table tipe_obat
-- ----------------------------
ALTER TABLE "public"."tipe_obat" ADD CONSTRAINT "tipe_obat_pkey" PRIMARY KEY ("id_tipe");

-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE "public"."user" ADD CONSTRAINT "user_pkey" PRIMARY KEY ("id_user");
